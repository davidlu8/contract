<?php

declare(strict_types=1);

namespace Contract\Exceptions;

use Exception;
use Throwable;

class AuthenticationException extends Exception
{
    protected array $data;

    public function __construct(string $message = '', int $code = 0, array $data = [], Throwable $previous = null)
    {
        $this->data = $data;

        parent::__construct($message, $code, $previous);
    }

    public function getData(): ?array
    {
        return $this->data;
    }
}