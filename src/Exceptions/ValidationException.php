<?php

declare(strict_types=1);

namespace Contract\Exceptions;

use Exception;

class ValidationException extends Exception
{

}